/* eslint-disable no-console */
const net = require('net');
const fs = require('fs');
const path = require('path');
const port = process.env.PORT || 3000;


const server = net.createServer();
server.on('connection', function(socket) {
  socket.on('data', data => {
    const inData = data.toString('utf-8');
    const url = inData.slice(0, inData.indexOf('\r\n')).split(' ')[1];
    const fileType = url.split('.')[1];
    const reqType = inData.slice(0, inData.indexOf('\r\n')).split(' ')[0];
    const headers = {};
    inData.slice(inData.indexOf('\r\n') + 2, inData.indexOf('\r\n\r\n'))
      .split('\r\n')
      .forEach(elem => {
        const helpArr = elem.split(': ');

        headers[helpArr[0]] = helpArr[1]; });

    console.log(JSON.stringify(headers));

    fs.readFile(path.join(__dirname, 'static', url), (err, file) => {
      if (err) {
        let error = '400 Bad Request';

        if (err.code === 'ENOENT') {
          error = '404 Page Not Found';
        }

        if (err.code === 'EACCES') {
          error = '403 Forbidden';
        }

        socket.write(`HTTP/1.1 ${error}\r\n\r\n${error}\r\n\r\n`);
      } else {
        let contentType = `text/${fileType}`;

        if (fileType === 'jpg' || fileType === 'png') {
          contentType = `image/${fileType}`;
        }

        socket.write(`HTTP/1.1 OK 200\r\nContent-Type: ${contentType}\r\n\r\n`);
        socket.write(file);
        socket.end();
      }
    });
  });
}
);

server.listen(port);
console.log(`!!!!!listening on port ${port} \n\r`);